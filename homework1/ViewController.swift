//
//  ViewController.swift
//  homework1
//
//  Created by Sereyvong Thorn on 10/31/18.
//  Copyright © 2018 Sereyvong Thorn. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPwd: UITextField!
    @IBOutlet weak var lbMsg: UILabel!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfUsername.delegate = self
        tfPwd.delegate = self
        
        tfUsername.layer.borderWidth = 0
        tfPwd.layer.borderWidth = 0
        
        tfUsername.layer.cornerRadius = 10
        tfPwd.layer.cornerRadius = 10
        
        
        btnLogin.layer.cornerRadius = 6
        btnClear.layer.cornerRadius = 6
        
    }
    
    @IBAction func loginButton(_ sender: Any) {
        login()
    }
    @IBAction func clearButton(_ sender: Any) {
        resetForm()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if ( textField == self.tfUsername ) {
            self.tfPwd.becomeFirstResponder()
        }
        if ( textField == self.tfPwd ) {
            login()
        }
        return true
    }
    
    func login(){
        let username = "admin"
        let password = "123"
        
        if tfUsername.text == "" {
            lbMsg.text = "please input username!"
            lbMsg.textColor = .yellow
        }else if tfPwd.text == "" {
            lbMsg.text = "please input password!"
            lbMsg.textColor = .yellow
        }else{
            if username != tfUsername.text || password != tfPwd.text {
                lbMsg.text = "Invalid username and password!"
                lbMsg.textColor = .red
            }else{
                lbMsg.text = "login successfully!"
                lbMsg.textColor = #colorLiteral(red: 0, green: 0.5568627451, blue: 0, alpha: 1)
            }
        }
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if ( textField == self.tfPwd ) {
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func resetForm(){
        lbMsg.text = "";
        tfUsername.text = ""
        tfPwd.text = ""
        self.view.endEditing(true)
        
    }
    
}

